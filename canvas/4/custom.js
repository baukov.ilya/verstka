function Animation(rate){
    this.Lasttime = 0;
    this.rate = rate;
    this.update = function(){};
    this.render = function(){};
}
Animation.prototype.run = function(time) {
    if((time - this.Lasttime)>this.rate){
        this.Lasttime = time;
        this.update();
    }
    this.render();
}

function mainloop(){
    var art = new Image();
    art.src = "art.png";
    art.onload = function(){ 
        requestAnimationFrame(mainloop);
    }
    
    
}

var canvas = document.getElementById('my_canvas');
var context = canvas.getContext('2d');

requestAnimationFrame(mainloop);