var canvas = document.getElementById('my_canvas');
canvas.width=400;
canvas.height=300;
var context = canvas.getContext('2d');
//Заливка 1 цветом
context.fillStyle = 'red';
context.fillRect(0,0,400,300);
//Градиент заливка
var grd = context.createLinearGradient(0,0,400,300);
grd.addColorStop(0,"red");//1-й цвет и его индекс
grd.addColorStop(1,"white");//2-й цвет
context.fillStyle = grd;
context.fillRect(0,0,400,300);
//Линии
context.beginPath();//начало пути
context.moveTo(0,0,);//ставим перо в точку
context.lineTo(400,0);//проводим линию до точки
context.moveTo(0,300);
context.lineTo(400,300);
context.stroke();//отрисовываем линию

//Круг
context.beginPath();
context.arc(50,50,30,0,2 * Math.PI);//x,y,radius,угл начала, конца,сторона оборота
context.stroke();

context.beginPath();
context.arc(300,50,30,0,Math.PI,true);
context.stroke();

//Текст
context.fillStyle = "black";//стиль заливки
context.font="30px Arial";//стиль текста
context.fillText("Tapok",50,150);//текст, координаты

//картинки
var img = document.getElementById("kek");
context.drawImage(img,50,200);

