let canvas = document.getElementById('my_canvas');
let context = canvas.getContext('2d');
let success = 0;

let scoreBar=document.getElementById('score');
let balls_types=[0,0,0,0,
                0,0,0,0];
let balls_x=[1000,1000,1000,1000,
            1000,1000,1000,1000];
let balls_y=[1000,1000,1000,1000,
            1000,1000,1000,1000];

var girl_x = 208;
var girl = new Image();
girl.src="girl.png";


function background(){
    
    context.fillStyle = "blue";
    context.fillRect(0,0,480,768);
}

function girladd(evt){
    
    
   if ((evt.keyCode===68 || evt.keyCode===39)&&(girl_x<410))
       {
           girl_x+=5;
       }
    
    if ((evt.keyCode===65 || evt.keyCode===37)&&(girl_x>5))
       {
           girl_x-=5;
       }
    context.drawImage(girl,0,0,64,64,
                     girl_x,702,64,64)
    
}

function balls(){
    var i=0;
    while(i<8)
        {
                if(((balls_y[i-1]>400 && balls_y[i-1]<700) || i===0)&& balls_y[i]>800)
                    {
                        balls_y[i]=64;
                        balls_x[i]=Math.floor(Math.random()*410);
                        balls_types[i]=Math.floor(Math.random()*2)+1;
                        
                    }  
            i++;
        }
        for(i=0;i<8;i++)
            {
                if(balls_y[i]>670 && balls_y[i]<832 && balls_x[i]>girl_x-64 && balls_x[i]<girl_x+64 ) {
                    balls_y[i]=1000; 
                    success++;
                    scoreBar.textContent='Score:'+success+'/100';
                }
                else if(balls_y[i]<1000 && balls_y[i]>0)
                    {
                        balls_y[i]+=5;
                        var ball = new Image();
                        ball.src=balls_types[i] + ".png"; 
                        context.drawImage(ball,0,0,64,64,
                                  balls_x[i],balls_y[i],64,64);
                    }
                
            }
    }


function loop(){
    if (success<100)
    {
        background();
   context.drawImage(girl,0,0,64,64,
                     girl_x,702,64,64);
    document.addEventListener('keydown',girladd);
    balls();
    }
    else
    {
    context.fillStyle = "yellow";
    context.fillRect(0,0,480,768);
    scoreBar.textContent="Good job!";
    }
}
    


setInterval(loop,30);

