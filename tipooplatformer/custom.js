let canvas=document.getElementById("my_canvas");
let context=canvas.getContext('2d');

let map_x=0;
let char_x=64;
let char_pos=2;
let char_y=322;
let char_map_place = char_x;
let flagup = true;
let flagdown = true;
let speed = 0;
let basespeed = 10;
var sprite_sheet = new Image();
sprite_sheet.src="pictures.png";

var map = [
           [8,1,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8],
           [8,1,8,8,1,8,8,8,8,8,1,1,8,8,8,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,1,1,8,1,1,8,8,8,8,8,8,8,8,8,1,8,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8],
           [8,1,8,8,1,8,8,8,8,1,1,8,8,1,1,1,1,8,8,1,8,8,8,8,8,1,8,8,8,8,8,8,8,8,8,1,8,1,8,8,8,8,8,1,8,8,1,1,8,8,1,8,8,8,8,8,8,8,8,8,8,8,1,8,1,1,8,1,8,8,8,8,8,8,8,8,8,8],
           [8,1,8,8,1,8,8,1,1,1,8,8,1,1,1,1,8,8,1,8,1,8,8,1,8,8,1,8,8,1,8,1,8,8,8,8,1,8,8,1,1,8,1,8,1,8,1,8,8,8,8,8,8,1,8,8,1,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8],
           [8,1,8,8,1,8,1,1,8,8,1,1,1,1,8,8,8,8,8,1,8,8,1,8,8,1,8,8,8,8,8,8,1,8,8,1,8,8,8,8,1,8,8,8,8,1,1,8,1,8,8,8,8,1,8,8,8,8,8,1,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8],
           [8,1,8,8,1,8,8,8,8,1,8,8,8,8,8,8,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,1,8,8,8,8,8,8,8,8,8,8,8,8,8,8,1,8,8,1,8,8,1,8,8,1,8,8,8,8,8,8,8,8,8],
           [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
          ];
function background(){
    context.fillStyle = "red";
    context.fillRect(0,0,640,448);
    for(var column = Math.ceil(map_x/64)+2;column<Math.ceil(map_x/64)+14;column++) {
        for (var row = 0; row < 7; row++) {
            if (map_x % 64 !== 0)
                context.drawImage(sprite_sheet,
                    map[row][column] * 64, 0, 64, 64,
                    column * 64 - Math.ceil(map_x / 64) * 64 - map_x % 64 - 128, row * 64, 64, 64);
            else
                context.drawImage(sprite_sheet,
                    map[row][column] * 64, 0, 64, 64,
                    column * 64 - Math.ceil(map_x / 64) * 64 - 64 - 128, row * 64, 64, 64);

            context.drawImage(sprite_sheet,
                char_pos * 64, 0, 64, 64,
                char_x, char_y, 64, 64)
        }
    }
    if(char_x > 400)
        map_x += speed;
}

function CharAdd(){
    char_map_place += speed;
    if(char_x<=410 && char_x>=5) {
        char_x += speed;
        context.drawImage(sprite_sheet, 0, 0, 64, 64,
            char_x, char_y, 64, 64);

        if (char_x > 411)
            char_x = 410;
        if (char_x < 4)
            char_x = 5;
    }
    else{
        map_x += speed;
    }
}
function speedup(evt){
    if ((evt.keyCode===68 || evt.keyCode===39)&&(char_x<=411)&&(speed<10)&&(flagup===true))
    {
        speed+=basespeed;
        flagup=false;
    }
    if ((evt.keyCode===65 || evt.keyCode===37)&&(char_x>=4)&&(speed>-10)&&(flagdown===true))
    {
        speed-=basespeed;
        flagdown=false;
    }
    if(char_pos < 5)
        char_pos++;
    else
        char_pos=char_pos-3;


}

function speeddown(evt){
    if ((evt.keyCode===68 || evt.keyCode===39)&&(char_x<=410)&&(speed>-10)&&(flagup===false))
    {
        speed-=basespeed;
        flagup=true;
    }
    if ((evt.keyCode===65 || evt.keyCode===37)&&(char_x>=5)&&(speed<10)&&(flagdown===false))
    {
        speed+=basespeed;
        flagdown=true;
    }
    context.drawImage(sprite_sheet, char_pos*64,0,64,64, char_x,char_y,64,64);
        {
            if(char_pos < 5)
            char_pos++;
           else
            char_pos=char_pos-3;   
        }
}




function jumpUp1(){
    char_y-=32;
    background();
    document.addEventListener('keydown',CharAdd);
    setTimeout(jumpUp2,20);
}
function jumpUp2(){
    char_y-=32; 
    background();
    document.addEventListener('keydown',CharAdd);
    setTimeout(jumpUp3,20);
}
function jumpUp3(){
    char_y-=32; 
    background();
    document.addEventListener('keydown',CharAdd);
    setTimeout(jumpUp4,20);
}
function jumpUp4(){
    char_y-=32; 
    background();
    document.addEventListener('keydown',CharAdd);
}
function jumpDown1(){
    char_y+=32; 
    background();
    document.addEventListener('keydown',CharAdd);
    setTimeout(jumpDown2,20);
}
function jumpDown2(){
    char_y+=32; 
    background();
    document.addEventListener('keydown',CharAdd);
    if (((map[Math.ceil(char_y/64)][Math.ceil(map_x/64)+Math.ceil(char_x/64)+3]===1
                         && char_x >= ((Math.ceil(map_x/64)+Math.ceil(char_x/64)+3)*64-192-map_x%64 && char_x <= (Math.ceil(map_x/64)+Math.ceil(char_x/64)+3)*64-128-map_x%64)))
                        ||
                        ((map[Math.ceil(char_y/64)][Math.ceil(map_x/64)+Math.ceil(char_x/64)+2]===1
                         && char_x >= ((Math.ceil(map_x/64)+Math.ceil(char_x/64)+2)*64-192-map_x%64 && char_x <= (Math.ceil(map_x/64)+Math.ceil(char_x/64)+2)*64-128-map_x%64))))
        {}
    else
    setTimeout(jumpDown3,20);
}
function jumpDown3(){
    char_y+=32;
    background();
    document.addEventListener('keydown',CharAdd);
    setTimeout(jumpDown4,20);
}
function jumpDown4(){
    char_y+=32; 
    background();
    document.addEventListener('keydown',CharAdd);
}


function jump(evt){
    
    if (evt.keyCode===32 || evt.keyCode===87 || evt.keyCode===38)
        {
           jumpUp1();
        }
    
}

function fall(){
    if(char_y < 300)
        if (((map[Math.ceil(char_y/64)][Math.ceil(map_x/64)+Math.ceil(char_x/64)+3]===1
            && char_x >= ((Math.ceil(map_x/64)+Math.ceil(char_x/64)+3)*64-192-map_x%64 && char_x <= (Math.ceil(map_x/64)+Math.ceil(char_x/64)+3)*64-128-map_x%64)))
            ||
            ((map[Math.ceil(char_y/64)][Math.ceil(map_x/64)+Math.ceil(char_x/64)+2]===1
                && char_x >= ((Math.ceil(map_x/64)+Math.ceil(char_x/64)+2)*64-192-map_x%64 && char_x <= (Math.ceil(map_x/64)+Math.ceil(char_x/64)+2)*64-128-map_x%64))))
        {}
    else
        setTimeout(jumpDown1,100);
}



function loop(){
    background();
    document.addEventListener('keydown',speedup);
    document.addEventListener('keyup',speeddown);
    CharAdd();
    context.drawImage(sprite_sheet,0,0,64,64,
        char_x,char_y,64,64);
     
}

sprite_sheet.onload = function(){
    setInterval(loop,10);
    
document.addEventListener('keydown',jump);
         setInterval(fall,200);
};



        
        



