if (document.getElementsByClassName("nav")[0]){
    console.log("++")
    let body = document.getElementsByTagName("body")[0];
    let nav_input = document.getElementsByClassName("nav__info-input")[0];
    let nav_search = document.getElementsByClassName("nav__info-search")[0];
    let nav_icon = document.getElementsByClassName("nav__info-user")[0];
    let nav_settings = document.getElementsByClassName("nav__info-settings")[0];
    let search_flag = false;
    let settings_flag = false;
    nav_input.addEventListener("click", function () {
        console.log("3");
        nav_search.classList.remove("hide_height");
        nav_search.classList.remove("hide_opacity");
        search_flag = true
    });
    nav_icon.addEventListener("click", function () {
        console.log("2");
        nav_settings.classList.toggle("hide_height");
        nav_settings.classList.toggle("hide_opacity");
        settings_flag = true;
    });
    nav_settings.addEventListener("click", function () {
        settings_flag = true
    })
    nav_search.addEventListener("click", function () {
        search_flag = true
    })
    body.addEventListener("click", function () {
        console.log(settings_flag);
        if (!search_flag){
            nav_search.classList.add("hide_height");
            nav_search.classList.add("hide_opacity");
        }
        if (!settings_flag){
            nav_settings.classList.add("hide_height");
            nav_settings.classList.add("hide_opacity");
        }
        settings_flag = false;
        search_flag = false;
    })
}




if(document.getElementsByClassName("main-page")[0]) {
    carousel_items_count = 12;
    $('.owl-carousel-rel').owlCarousel({
        loop: false,
        margin: 14,
        nav: true,
        items: 6,
        dots: false,
        slideBy: 3,
        autoWidth: true,
        onDragged: first_last_carousel

    });
    $('.owl-carousel-ytb').owlCarousel({
        loop: false,
        margin: 14,
        nav: true,
        items: 2,
        dots: false,
        slideBy: 3,
        autoWidth: true,

    });

    $(document).ready(function () {
        $(".owl-carousel").owlCarousel();
    });


    function first_last_carousel() {
        let width_of_elem = 195;
        let margin = 14;
        let window_width = document.documentElement.clientWidth;
        let sliders = document.getElementsByClassName("owl-carousel-rel");
        let carousels_start_pos = [];
        for (let i = 0; i < sliders.length; i++) {
            let transform = sliders[i].getElementsByClassName("owl-stage")[0].style.transform.split(/\w+\(|\);?/)[1].split(/,\s?/g).map(parseInt);
            carousels_start_pos[i] = (-1) * transform[0];
        }
        carousels_start_pos = carousels_start_pos.map((item) => {
            if (item / (margin + width_of_elem) !== 0 && item / (margin + width_of_elem) !== -0)
                return item / (margin + width_of_elem);
            else return 0
        });
        let articles = [];
        for (let i = 0; i < sliders.length; i++) {
            articles[i] = sliders[i].getElementsByClassName("owl-item")
        }
        for (let j = 0; j < articles.length; j++) {
            for (let i = 0; i < carousel_items_count; i++) {
                articles[j][i].classList.remove("first_carousel_main-rel");
                articles[j][i].classList.remove("last_carousel_main-rel");
            }
        }
        for (let i = 0; i < carousels_start_pos.length; i++) {
            articles[i][carousels_start_pos[i]].classList.add("first_carousel_main-rel");
            articles[i][carousels_start_pos[i] + 5].classList.add("last_carousel_main-rel");
        }
        // let items = document.getElementsByClassName("owl-item");
        // for(let i = 0; i < items.length; i++){
        //     items[i].style.marginLeft = "0";
        // }
    }

    window.addEventListener("resize", function () {
        first_last_carousel()
    });
    document.addEventListener("DOMContentLoaded", function () {
        buttons = document.getElementsByClassName("owl-next");
        buttonsPr = document.getElementsByClassName("owl-prev");
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", function () {
                first_last_carousel();
            })
        }
        for (let i = 0; i < buttonsPr.length; i++) {
            buttonsPr[i].addEventListener("click", function () {
                first_last_carousel();
            })
        }

        let sliders = document.getElementsByClassName("owl-stage-outer");
        for (let i = 0; i < sliders.length; i++) {
            sliders[i].style.overflow = "visible";
        }
    });
    first_last_carousel();
}
if(document.getElementsByClassName("release__page")[0]){
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 14,
        nav: true,
        items: 3,
        dots: false,
        slideBy: 3,
        autoWidth: false
    })

    function showDescription() {
        let description_container = document.getElementsByClassName("release__info-description__container")[0];
        description_container.classList.toggle("release__info-description__container-show");
        let description_btn = document.getElementsByClassName("release__info-description__button")[0];
        setTimeout(function () {
            description_btn.classList.toggle("release__info-description__button-show");
        }, 150);
        let text = description_btn.getElementsByTagName("span");
        text[0].classList.toggle("hide");
        text[1].classList.toggle("hide")
    }
    document.addEventListener("DOMContentLoaded", function () {
        let showbtn = document.getElementsByClassName("release__comments-items-comment-content-answers-btn");
        let hidebtn = document.getElementsByClassName("release__comments-items-comment-content-answers-btn-hide");
        let answer = document.getElementsByClassName("release__comments-items-comment-content-answers");
        for (let i = 0; i < showbtn.length; i++){
            showbtn[i].addEventListener("click", function () {
                subcomment = answer[i].getElementsByClassName("release__comments-items-comment-content-answers-container")[0];
                showbtn[i].classList.toggle("hide");
                hidebtn[i].classList.toggle("hide");
                subcomment.classList.toggle("hide");
            });
            hidebtn[i].addEventListener("click", function () {
                subcomment = answer[i].getElementsByClassName("release__comments-items-comment-content-answers-container")[0];
                showbtn[i].classList.toggle("hide");
                hidebtn[i].classList.toggle("hide");
                subcomment.classList.toggle("hide");
            });
        }
    });
    function auto_grow(element) {
        element.style.height = "5px";
        element.style.height = (element.scrollHeight)+ 1 + "px" ;
        let parent_el = element.parentElement;
        parent_el.getElementsByClassName("release__comments-form-input-emoji")[0].style.height = (element.scrollHeight)+"px";
        if (element.value.length >= 500){
            parent_el.getElementsByClassName("release__comments-form-input-length")[0].classList.remove("hide");
            parent_el.getElementsByClassName("release__comments-form-input-length")[0].textContent = 1024 - element.value.length ;
        }
        else
            parent_el.getElementsByClassName("release__comments-form-input-length")[0].classList.add("hide");
    }
    document.addEventListener("DOMContentLoaded", function () {

        let reply_btns = document.getElementsByClassName("release__comments-items-comment-content-info-svg-reply");
        let textarea = document.getElementsByTagName("textarea");
        let forms = document.getElementsByClassName("release__comments-form");
        auto_grow(textarea[0]);
        for (let i = 0; i < reply_btns.length; i++) {
            reply_btns[i].addEventListener("click", function () {
                forms[i + 1].classList.remove("hide");
                textarea[i + 1].placeholder = "Ваш ответ...";
                auto_grow(textarea[i + 1])
            })
            auto_grow(textarea[i]);
        }
    })

    function scroll_updates() {
        let YstartPos = document.body.scrollHeight - 671 - document.getElementsByClassName("release__comments-container")[0].offsetHeight - 81;
        let YoffSet =  window.pageYOffset;
        console.log(YstartPos);
        if (YoffSet > YstartPos && document.body.scrollHeight - YoffSet >= 1213)
            document.getElementsByClassName("release__comments-updates")[0].style.marginTop = YoffSet - YstartPos + "px";
        else if (YoffSet < YstartPos)
            document.getElementsByClassName("release__comments-updates")[0].style.marginTop = "0px"
    }
    document.getElementsByTagName("body")[0].onscroll = scroll_updates;

    let player_btns = document.getElementsByClassName("release__video-player-header-players")[0].getElementsByClassName("release__video-player-header-players-player");
    document.addEventListener("DOMContentLoaded", function () {
        for (let i = 0; i < player_btns.length; i++){
            player_btns[i].addEventListener("click", function () {
                for (let j = 0; j < player_btns.length; j++)
                    player_btns[j].classList.remove("active");
                player_btns[i].classList.add("active");

            })
        }
        let spoiler_btn = document.getElementsByClassName("release__comments-items-comment-content-spoiler");
        for (let i = 0; i < spoiler_btn.length; i++){
            spoiler_btn[i].parentElement.getElementsByClassName("release__comments-items-comment-content-text")[0].classList.add("hide");
            spoiler_btn[i].addEventListener("click", function () {
                spoiler_btn[i].parentElement.getElementsByClassName("release__comments-items-comment-content-text")[0].classList.remove("hide");
                spoiler_btn[i].classList.add("hide");
            })
        }
        let comments_more_btn = document.getElementsByClassName("release__comments-more")[0];
        let preloader = document.getElementsByClassName("preloader")[0];
        comments_more_btn.addEventListener("click", function () {
            preloader.classList.remove("hide");
        });
        let player_container = document.getElementsByClassName("release__video-player")[0];
        let light_btn = document.getElementsByClassName("light")[0];
        let light_shadow = document.getElementsByClassName("release__video__light-shadow")[0];
        light_btn.addEventListener("click", function () {
            light_btn.classList.toggle("light__active");
            light_shadow.classList.toggle("hide_modal");
            player_container.classList.toggle("release__video-player-active");
            if (light_shadow.classList.contains("hide_modal"))
                light_btn.getElementsByTagName("span")[0].textContent = "Выключить свет";
            else
                light_btn.getElementsByTagName("span")[0].textContent = "Включить свет";

        })
        light_shadow.addEventListener("click", function () {
            light_btn.classList.toggle("light__active");
            light_shadow.classList.toggle("hide_modal");
            player_container.classList.toggle("release__video-player-active");
            if (light_shadow.classList.contains("hide_modal"))
                light_btn.getElementsByTagName("span")[0].textContent = "Выключить свет";
            else
                light_btn.getElementsByTagName("span")[0].textContent = "Включить свет";

        })
        let comments_btn = document.getElementsByClassName("release__comments-items-header")[0].getElementsByTagName("button");
        for (let i = 0; i < comments_btn.length; i++){
            comments_btn[i].addEventListener("click", function () {
                for (let j = 0; j < comments_btn.length; j++)
                    comments_btn[j].classList.remove("active");
                comments_btn[i].classList.add("active");
            })
        }


    let modal_video = document.getElementsByClassName("modal__video")[0];
    let modal_video_btn = document.getElementsByClassName("modal__video-close")[0];
    let trailer_btn = document.getElementsByClassName("release__info-content-trailer")[0];
    modal_video_btn.addEventListener("click", function () {
        modal_video.classList.add("hide_modal");
    });
    trailer_btn.addEventListener("click", function () {
        modal_video.classList.remove("hide_modal");
    });
    let screenshots_btn = document.getElementsByClassName("release__info-content-trailer")[1];
    let modal_screenshots = document.getElementsByClassName("modal__screenshots")[0];
    let modal_screenshots_btn = document.getElementsByClassName("modal__screenshots-close")[0];
    modal_screenshots_btn.addEventListener("click", function () {
        modal_screenshots.classList.add("hide_modal");
    });
    screenshots_btn.addEventListener("click", function () {
        modal_screenshots.classList.remove("hide_modal");
    });

    let screenshots_items = document.getElementsByClassName("modal__screenshots-carousel-content-menu-item");
    let current_element = 0;
    let el_numbers = screenshots_items.length;
    let screenshot_img = document.getElementsByClassName("modal__screenshots-carousel-content-cur")[0].getElementsByTagName("img")[0];
    for (let i = 0; i < el_numbers; i++)
        screenshots_items[i].addEventListener("click", function () {
            screenshots_items[current_element].classList.remove("active");
            screenshots_items[i].classList.add("active");
            current_element = i;
            screenshot_img.src = screenshots_items[current_element].getElementsByTagName("img")[0].src;
        });

    let next_btn = document.getElementsByClassName("modal__screenshots-carousel-content-next")[0];
    let prev_btn = document.getElementsByClassName("modal__screenshots-carousel-content-prev")[0];
    next_btn.addEventListener("click", function () {
        screenshots_items[current_element].classList.remove("active");
        current_element = current_element + 1;
        if (current_element >= el_numbers)
            current_element = 0;
        screenshots_items[current_element].classList.add("active");
        screenshot_img.src = screenshots_items[current_element].getElementsByTagName("img")[0].src;
    });
    prev_btn.addEventListener("click", function () {
        screenshots_items[current_element].classList.remove("active");
        current_element = current_element - 1;
        if (current_element < 0)
            current_element = el_numbers - 1;
        screenshots_items[current_element].classList.add("active");
        screenshot_img.src = screenshots_items[current_element].getElementsByTagName("img")[0].src;
    });
    let modal__close = document.getElementsByClassName("modal__close");
    for (let i = 0; i < modal__close.length; i++){
        modal__close[i].addEventListener("click", function () {
            modal_screenshots.classList.add("hide_modal");
            modal_video.classList.add("hide_modal");
        })
    }
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            modal_screenshots.classList.add("hide_modal");
            modal_video.classList.add("hide_modal");
        }
        if (e.key === "ArrowRight"){
            screenshots_items[current_element].classList.remove("active");
            current_element = current_element + 1;
            if (current_element >= el_numbers)
                current_element = 0;
            screenshots_items[current_element].classList.add("active");
            screenshot_img.src = screenshots_items[current_element].getElementsByTagName("img")[0].src;
        }
        if (e.key === "ArrowLeft"){
            screenshots_items[current_element].classList.remove("active");
            current_element = current_element - 1;
            if (current_element < 0)
                current_element = el_numbers - 1;
            screenshots_items[current_element].classList.add("active");
            screenshot_img.src = screenshots_items[current_element].getElementsByTagName("img")[0].src;
        }
    });
    });

    let vid_players = document.getElementsByClassName("release__video-player-video");
    let vid_btns = document.getElementsByClassName("release__video-player-header-players-player");
    document.addEventListener("DOMContentLoaded", function () {
        for(let i = 0; i < vid_btns.length - 2; i++){
            vid_btns[i].addEventListener("click", function () {
                for (let j = 0; j < vid_players.length; j++){
                    vid_players[j].classList.add("hide");
                    vid_btns[j].classList.remove("active");
                }
                vid_players[i].classList.remove("hide");
                vid_btns[i].classList.add("active")
            })
        }
    })
    
}
    // if(typeof player === 'undefined' && $('div#moonPlayer iframe').length == 0){
    //     $('#buttonAni').hide();
    //     $('#buttonMoon').hide();
    // }else{
    //     tabSwitch('anilibriaPlayer');
    //     if($('div#moonPlayer iframe').length > 0){
    //         $('#buttonMoon').show();
    //         if(typeof player === 'undefined'){
    //             tabSwitch('moonPlayer');
    //             $('#buttonAni').hide();
    //         }
    //     }
    // }
    // function tabSwitch(tab) {
    //     $('[data-tab]').removeClass("active");
    //     $('[data-tab='+ tab + "]").addClass("active");
    //     $('.xplayer').hide();
    //     $('#' + tab).show()
//
// }