menu = document.getElementsByClassName('show_menu_button');
let x = document.getElementById('mynav');
menu[0].addEventListener('click',function () {

    if(x.className ==="nav"){
        x.className +=" responsive";
    }
    else
        x.className = "nav"

});

let accord_buttons_on = document.getElementsByClassName("fa-sort-down");
let accord_buttons_off = document.getElementsByClassName("fa-sort-up");
let accord_text_areas = document.getElementsByTagName("textarea");
let i = 0;
let actived = 0;
    accord_buttons_on[0].addEventListener("click", function () {
        accord_text_areas[0].classList.toggle("service2_textarea_active");
        accord_buttons_off[0].classList.toggle("service2_active");
        accord_buttons_on[0].classList.toggle("service2_active");
        if(actived !== -1) {
            accord_text_areas[actived].classList.toggle("service2_textarea_active");
            accord_buttons_off[actived].classList.toggle("service2_active");
            accord_buttons_on[actived].classList.toggle("service2_active");
        }
        actived = 0;
    });
    accord_buttons_off[0].addEventListener("click", function () {
        accord_text_areas[0].classList.toggle("service2_textarea_active");
        accord_buttons_off[0].classList.toggle("service2_active");
        accord_buttons_on[0].classList.toggle("service2_active");
        actived = -1;
    });

    accord_buttons_on[1].addEventListener("click", function () {
        accord_text_areas[1].classList.toggle("service2_textarea_active");
        accord_buttons_off[1].classList.toggle("service2_active");
        accord_buttons_on[1].classList.toggle("service2_active");
        if(actived !== -1) {
            accord_text_areas[actived].classList.toggle("service2_textarea_active");
            accord_buttons_off[actived].classList.toggle("service2_active");
            accord_buttons_on[actived].classList.toggle("service2_active");
        }
        actived = 1;
    });
    accord_buttons_off[1].addEventListener("click", function () {
        accord_text_areas[1].classList.toggle("service2_textarea_active");
        accord_buttons_off[1].classList.toggle("service2_active");
        accord_buttons_on[1].classList.toggle("service2_active");
        actived = -1;
    });

    accord_buttons_on[2].addEventListener("click", function () {
        accord_text_areas[2].classList.toggle("service2_textarea_active");
        accord_buttons_off[2].classList.toggle("service2_active");
        accord_buttons_on[2].classList.toggle("service2_active");
        if(actived !== -1){
            accord_text_areas[actived].classList.toggle("service2_textarea_active");
            accord_buttons_off[actived].classList.toggle("service2_active");
            accord_buttons_on[actived].classList.toggle("service2_active");
        }
        actived = 2;
    });
    accord_buttons_off[2].addEventListener("click", function () {
        accord_text_areas[2].classList.toggle("service2_textarea_active");
        accord_buttons_off[2].classList.toggle("service2_active");
        accord_buttons_on[2].classList.toggle("service2_active");
        actived = -1;
    });

    let our_team_icons = document.getElementsByClassName("our_team_icon");
    let our_team_members = document.getElementsByClassName("our_team_pic");
    our_team_members[0].addEventListener('mouseover',function () {
        for(let i = 0; i<4;i++)
        our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });
    our_team_members[1].addEventListener('mouseover',function () {
        for(let i = 4; i<8;i++)
            our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });
    our_team_members[2].addEventListener('mouseover',function () {
        for(let i = 8; i<12;i++)
            our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });
    our_team_members[0].addEventListener('mouseout',function () {
        for(let i = 0; i<4;i++)
            our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });
    our_team_members[1].addEventListener('mouseout',function () {
        for(let i = 4; i<8;i++)
            our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });
    our_team_members[2].addEventListener('mouseout',function () {
        for(let i = 8; i<12;i++)
            our_team_icons[i].classList.toggle("our_team_pic_i_active")
    });

let gallery_hidden = document.getElementsByClassName("hidden_gallery_element_unactive");
let gallery_elem = document.getElementsByClassName("gallery_element");
let gallery_big_elem = document.getElementsByClassName("gallery_big_element");
let close_button = document.getElementsByClassName("close_button")

gallery_elem[0].addEventListener('click',function () {
    gallery_hidden[0].classList.toggle("hidden_gallery_element_active");
});
gallery_elem[1].addEventListener('click',function () {
    gallery_hidden[1].classList.toggle("hidden_gallery_element_active");
});
gallery_elem[2].addEventListener('click',function () {
    gallery_hidden[2].classList.toggle("hidden_gallery_element_active");
});
gallery_elem[3].addEventListener('click',function () {
    gallery_hidden[3].classList.toggle("hidden_gallery_element_active");
});
gallery_elem[4].addEventListener('click',function () {
    gallery_hidden[4].classList.toggle("hidden_gallery_element_active");
});
gallery_elem[5].addEventListener('click',function () {
    gallery_hidden[5].classList.toggle("hidden_gallery_element_active");
});
gallery_big_elem[0].addEventListener('click',function () {
    gallery_hidden[6].classList.toggle("hidden_gallery_element_active");
});
close_button[0].addEventListener('click',function () {
    gallery_hidden[0].classList.toggle("hidden_gallery_element_active");
});
close_button[1].addEventListener('click',function () {
    gallery_hidden[1].classList.toggle("hidden_gallery_element_active");
});
close_button[2].addEventListener('click',function () {
    gallery_hidden[2].classList.toggle("hidden_gallery_element_active");
});
close_button[3].addEventListener('click',function () {
    gallery_hidden[3].classList.toggle("hidden_gallery_element_active");
});
close_button[4].addEventListener('click',function () {
    gallery_hidden[4].classList.toggle("hidden_gallery_element_active");
});
close_button[5].addEventListener('click',function () {
    gallery_hidden[5].classList.toggle("hidden_gallery_element_active");
});
close_button[6].addEventListener('click',function () {  
    gallery_hidden[6].classList.toggle("hidden_gallery_element_active");
});
let scroll_flag = false;
header = document.getElementsByTagName('header');
function Scroll_lol() {
    console.log(window.pageYOffset);
    if (window.pageYOffset > 800 && !scroll_flag){
        header[0].classList.toggle('nav_scrolled');
        scroll_flag = !scroll_flag;
    }
    else if(scroll_flag && window.pageYOffset < 800){
        header[0].classList.toggle('nav_scrolled');
        scroll_flag = !scroll_flag;
    }
}

Scroll_lol();